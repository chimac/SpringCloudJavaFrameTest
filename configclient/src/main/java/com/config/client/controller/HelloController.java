package com.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author machi
 */
@RefreshScope
@RestController
public class HelloController{

//    @Value("${damao}")
//    String name;
//
//    @RequestMapping(value = "/hello")
//    public String hello(){
//        return this.name;
//    }

    @Value("${logging.path}")
    private String message;

    @GetMapping("/test")
    public String test() {
        return message;
    }
}
