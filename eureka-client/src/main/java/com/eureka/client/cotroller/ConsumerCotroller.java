package com.eureka.client.cotroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerCotroller {
    @Autowired
    private RestTemplate template;

    @GetMapping("/test")
    public String index() {
        return template.getForEntity("http://localhost:8089/damao", String.class).getBody();
    }

    @GetMapping("/hello")
    public String hello() {
        return template.getForEntity("http://localhost:8089/hello", String.class).getBody();
    }
}
