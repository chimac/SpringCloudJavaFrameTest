package com.eureka.toserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
@EnableEurekaClient
@SpringBootApplication
public class ToserverApplication {


    public static void main(String[] args) {
        SpringApplication.run(ToserverApplication.class, args);
    }

}

